var gulp = require('gulp'),
sass = require('gulp-sass'),
notify = require("gulp-notify"),
jade = require ("gulp-jade"),
concat = require ("gulp-concat"),
uglify = require ("gulp-uglify"),
bower = require('gulp-bower'),
rename = require ('gulp-rename');
var gulp_watch_jade = require('gulp-watch-jade');


var config = {
  sassPath: './www/public/sass',
  bowerDir: './www/compnents'
}

gulp.task('js', function () {
  gulp.src('/www/public/js/*.js')
  .pipe(concat('main.js'))
  .pipe(uglify())
  .pipe(gulp.dest('/www/public/js/build/'))
});

gulp.task('bower', function() {
  return bower()
  .pipe(gulp.dest(config.bowerDir))
});

gulp.task('icons', function() {
  return gulp.src(config.bowerDir + '/font-awesome/fonts/**.*')
  .pipe(gulp.dest('./www/public/fonts'));
});

gulp.task('jade', function() {
  gulp.src('./www/jade/*.jade')
  .pipe(jade({
    locals: {
      title: 'ESPROVEN 2018'
    },
    pretty: true
  }))
  .pipe(gulp.dest('./'));
});

gulp.task('css', function() {
  gulp.src('./www/public/sass/*.sass', {style: 'compressed'})
  .pipe(rename({suffix: '.min'}))
  .pipe(sass().on('error', sass.logError))
  .pipe(gulp.dest('./www/public/css/'));

});
// Rerun the task when a file changes

gulp.task('watch', function() {
  gulp.watch(config.sassPath + './www/public/sass/main.sass', ['css']);
  gulp.src('jade/**/*.jade')
    .pipe(watch('jade/**/*.jade'))
    .pipe(gulp_watch_jade('jade/**/*.jade', { delay: 100 }))
    .pipe(jade())
    .pipe(gulp.dest('html/'));
});


gulp.task('default', ['js', 'jade', 'bower', 'icons', 'css']);
