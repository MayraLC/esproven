new Vue({
  el: '#appesproven',
  data: function() {
    return {
      menuitems: [
        {
          text: 'Inicio',
          url: 'index.html'
        },
        {
          text: '¿Quiénes Somos?',
          url: 'about-us.html'
        },
        {
          text: 'Instructores',
          url: 'profesors.html'
        },
        {
          text: 'Eventos',
          url: 'events.html'
        },
        {
          text: 'Programas',
          url: 'programs.html'
        },
        {
          text: 'Galería',
          url: 'gallery.html'
        },
        {
          text: 'MeOnline',
          url: 'http://www.meonline.com.mx'
        },
        {
          text: 'Contacto',
          url: 'contact.html'
        }
      ]
    };
  }
});
