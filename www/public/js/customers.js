var swiper = new Swiper('#customers', {
        autoplay: 1500,
     spaceBetween: 30,
     slidesPerView: 5,
        loop: true,
     breakpoints: {
            770: {
                slidesPerView: 2,
                spaceBetween: 15
            },
            500: {
                slidesPerView: 1,
                spaceBetween: 10
            }
        }
    });
